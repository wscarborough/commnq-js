/**
 * Commnq-js HttpHandler
 * This handler makes and forwards AMQP messages it receives
 * to a third party as an HTTP request.
 *
 * The specific HTTP request to be made can be configured by
 * passing an "httpOptions" object in the options for this
 * handler.  This object should contain the options that will
 * be delegated to http.request to make the actual request.
 */

'use strict';

var request = require('request')
  , util = require('util')
  , DefaultHandler = require('./default-handler');

var HttpHandler = function(options) {
  if (! (this instanceof HttpHandler)) {
    return new HttpHandler(options);
  }

  this.httpOptions = options.httpOptions;

  DefaultHandler.call(this, options);
};

util.inherits(HttpHandler, DefaultHandler);

HttpHandler.prototype.processMessage = function(message) {
  request.post({
    uri: (this.httpOptions.port === 443 ? 'https://' : 'http://') + this.httpOptions.host + this.httpOptions.path
    , json: message
    , auth: this.httpOptions.auth
  }, function(err) {
    if (err) {
      throw err;
    }
  });
};

module.exports = HttpHandler;