/**
 * DefaultHandler - the default Commnq-js handler from which other handlers should inherit.
 * Author: Matthew R Hanlon <mrhanlon@tacc.utexas.edu>
 */

'use strict';

var _ = require('underscore');

var DefaultHandler = function(options) {
  if (! (this instanceof DefaultHandler)) {
    return new DefaultHandler(options);
  }

  this.exchange = options.exchange;
  this.routingKey = options.routingKey || '#';
  this.options = options;

  _.bindAll(this, 'queueName', 'onMessage');
};

DefaultHandler.prototype.queueName = function() {
  return 'DefaultHandler' + new Date().getTime() + '#' + this.exchange;
};

DefaultHandler.prototype.queueOptions = function() {
  return {
    exclusive: true
  };
};

DefaultHandler.prototype.beforeProcessMessage = function(message) {
  return message;
};

DefaultHandler.prototype.processMessage = function(message) {
  console.log(message);
};

DefaultHandler.prototype.afterProcessMessage = function() {
  // no-op
};

DefaultHandler.prototype.onMessage = function(rawMessage, headers, deliveryInfo) {
  try {
    // parse raw message to JSON
    var message = JSON.parse(rawMessage.data.toString());

    // Any preprocessing of the message
    message = this.beforeProcessMessage(message, deliveryInfo, rawMessage, headers);

    // Do the main process message routing
    this.processMessage(message, deliveryInfo, rawMessage, headers);

    // Any postprocessing of the message
    this.afterProcessMessage(message, deliveryInfo, rawMessage, headers);
  } catch (err) {
    console.log(err);
  }
};

module.exports = DefaultHandler;