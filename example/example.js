'use strict';

var Commnq = require('commnq')
  , util = require('util')
  , _ = require('underscore');

var MyQueueHandler = function() {
  Commnq.Handlers.DefaultHandler.call(this, {exchange: 'glue2.activities'});
};
util.inherits(MyQueueHandler, Commnq.Handlers.DefaultHandler);

MyQueueHandler.prototype.beforeProcessMessage = function(message, deliveryInfo) {
  var states = _.groupBy(message.ComputingActivity, function(job) { return job.State[0]; })
    , queue = {};
  _.each(states, function(list, state) { queue[state] = list.length; });
  return { hostname: deliveryInfo.routingKey, queue: queue };
};

var params = { host:'amqp.example.com', port:5672, login:'guest', password:'guest' };

var server = new Commnq(params).connect(function() {
  console.log('Commnq ready!');
  server.registerHandler(new MyQueueHandler());
})